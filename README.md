
# Live network updates

As of version 0.2, Tallyphant can send live updates across the network,
allowing (for example) a remote large-screen scoreboard to be kept up
to date.

To enable this, just fill in the relevant fields on the Preferences
screen. Data is sent via UDP, in a very simple format. There is a test
server included with the source code, written in Python, that receives
and displays the data.

# Changelog

## Version 0.4

* A better fix for the bug fixed in 0.3 - this still happened on some Android versions.
* Got rid of occasional blank 'toast' messages, e.g. when resetting all items from the menu.

## Version 0.3

* Fixed a nasty bug that caused the new 'add any amount' functionality to sometimes update the wrong item.

## Version 0.2

* Configurable button modes (per item)
* Configurable label display styles
* Support for plurals
* Support for sharing results (uses ACTION_SEND, so email, microblog, whatever else is supported by installed apps)
* Menu option to reset all counters at once
* [[Networking]] support, for live updates.

## Version 0.1

* Initial release


